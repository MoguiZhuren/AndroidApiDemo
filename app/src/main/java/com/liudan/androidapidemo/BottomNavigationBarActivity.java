package com.liudan.androidapidemo;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;

/**
 * Created by liudan on 16/5/11.
 */
public class BottomNavigationBarActivity extends AppCompatActivity implements BottomNavigationBar.OnTabSelectedListener {

    private LocationFragment mLocationFragment;
    private LocationFragment mLocationFragment2;
    private LocationFragment mLocationFragment3;
    private LocationFragment mLocationFragment4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bottomnavigationbar);



        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);

        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.side_nav_bar, "位置"))
                .addItem(new BottomNavigationItem(R.drawable.side_nav_bar, "发现"))
                .addItem(new BottomNavigationItem(R.drawable.side_nav_bar, "爱好"))
                .addItem(new BottomNavigationItem(R.drawable.side_nav_bar, "图书"))
                .initialise();

        bottomNavigationBar.setTabSelectedListener(this);
        setDefaultFragment();
    }

    /**
     * 设置默认的
     */
    private void setDefaultFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        mLocationFragment = LocationFragment.newInstance("位置");
        transaction.replace(R.id.tabs, mLocationFragment);
        transaction.commit();
    }

    @Override
    public void onTabSelected(int position) {

//        Log.d(TAG, "onTabSelected() called with: " + "position = [" + position + "]");
        FragmentManager fm = this.getFragmentManager();
        //开启事务
        FragmentTransaction transaction = fm.beginTransaction();
        switch (position) {
            case 0:
                if (mLocationFragment == null) {
                    mLocationFragment = LocationFragment.newInstance("位置");
                }
                transaction.replace(R.id.tabs, mLocationFragment);
                break;
            case 1:
                if (mLocationFragment2 == null) {
                    mLocationFragment2 = LocationFragment.newInstance("发现");
                }
                transaction.replace(R.id.tabs, mLocationFragment2);
                break;
            case 2:
                if (mLocationFragment3 == null) {
                    mLocationFragment3 = LocationFragment.newInstance("爱好");
                }
                transaction.replace(R.id.tabs, mLocationFragment3);
                break;
            case 3:
                if (mLocationFragment4 == null) {
                    mLocationFragment4 = LocationFragment.newInstance("图书");
                }
                transaction.replace(R.id.tabs, mLocationFragment4);
                break;
            default:
                break;
        }
        // 事务提交
        transaction.commit();

    }

    @Override
    public void onTabUnselected(int position) {

    }

    @Override
    public void onTabReselected(int position) {

    }
}
