package com.liudan.androidapidemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by liudan on 16/5/5.
 */
public class ApiActivity extends Activity implements View.OnClickListener{

    private TextView tv_CoordinatorLayout;
    private TextView tv_DrawerLayout;
    private TextView tv_BottomNavigationBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);

        tv_CoordinatorLayout = (TextView)findViewById(R.id.tv_CoordinatorLayout);
        tv_DrawerLayout = (TextView)findViewById(R.id.tv_DrawerLayout);
        tv_BottomNavigationBar = (TextView)findViewById(R.id.tv_BottomNavigationBar);

        tv_CoordinatorLayout.setOnClickListener(this);
        tv_DrawerLayout.setOnClickListener(this);
        tv_BottomNavigationBar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.tv_DrawerLayout:
                intent = new Intent(this,DrawerLayoutActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_CoordinatorLayout:
                intent = new Intent(this,CoordinatorLayoutActivity.class);
                startActivity(intent);
        case R.id.tv_BottomNavigationBar:
                intent = new Intent(this,BottomNavigationBarActivity.class);
                startActivity(intent);
        }
    }
}
